package enums;

public enum StatusCode{

    SUCCESS(200),
    BAD_REQUEST(400),
    UNAUTHORIZED(401),
    FORBIDEN(403),
    NOT_FOUND(404),
    Unprocessable_Entity(422);

    private final int code;

    StatusCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
