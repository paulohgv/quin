package enums;

public enum RequestType {
    POST,
    GET,
    PUT,
    DELETE;
}
