package pojo.request;

public class BinRequest {

    public BinRequest(String sample){
        this.setSample(sample);
    }

    public BinRequest(){
    }

    public String sample;

    public void setSample(String sample) {
        this.sample = sample;
    }
}
