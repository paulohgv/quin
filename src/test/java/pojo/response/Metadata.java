package pojo.response;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Metadata {

    private String id;

    private Boolean Private;

    @JsonIgnore
    private String createdAt;

    @JsonIgnore
    private String message;

    @JsonIgnore
    private int versionsDeleted;

    Metadata(){}

    public String getId() {
        return id;
    }

    public void setId(String id) { this.id = id; }

    public Boolean getPrivate() { return Private;}

    public void setPrivate(Boolean aPrivate) {
        Private = aPrivate;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getMessage() { return message;}

    public void setMessage(String message) {this.message = message;}

    public int getVersionsDeleted() {return versionsDeleted; }

    public void setVersionsDeleted(int versionsDeleted) { this.versionsDeleted = versionsDeleted; }
}
