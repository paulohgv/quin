package pojo.response;

public class Record {

    private Record record;
    private String sample;
    private Metadata metadata;

    public void setError(Error error) {
        this.error = error;
    }

    private Error error;

    Record() {}

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public String getSample() {
        return sample;
    }

    public void setSample(String sample) {
        this.sample = sample;
    }

    public Record getRecord() {
        return record;
    }

    public void setRecord(Record record) {
        this.record = record;
    }



}
