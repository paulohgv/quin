package pojo.response;

public class Error {

    private String message;

    Error(){}

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
