package endpoints;

import enums.RequestType;
import io.restassured.response.Response;

public class DeleteBins extends BaseEndpoint {

    private String binId;

    public DeleteBins(String binId){
        setBinId(binId);
    }

    public String getBinId() {
        return binId;
    }

    public void setBinId(String binId) {
        this.binId = binId;
    }

    public Response deleteBinById(Boolean isFakeKey){
        return sendRequest(RequestType.DELETE, null, getBinId(), isFakeKey);
    }

}
