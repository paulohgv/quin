package endpoints;

import enums.RequestType;
import io.restassured.response.Response;
import pojo.request.BinRequest;

public class PutBins extends BaseEndpoint {

    private String binId;

    public PutBins(String binId){
        setBinId(binId);
    }

    public String getBinId() {
        return binId;
    }

    public void setBinId(String binId) {
        this.binId = binId;
    }

    public Response editBinById(Object bin, Boolean isFakeKey)
    {
        return sendRequest(RequestType.PUT, bin, getBinId(), isFakeKey);
    }
}
