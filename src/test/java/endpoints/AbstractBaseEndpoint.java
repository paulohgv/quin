package endpoints;

public abstract class AbstractBaseEndpoint {

    public static final String baseUrl = "https://api.jsonbin.io/v3";
    public static final String route = "/b/";
    public static final String API_KEY = "$2b$10$ILHIz2VzfQQMVgh5NaW6n.2yqUk7CBZmg2opCUOJIB03cuYZbLfa6";
    public static final String FAKE_KEY = "ABC";



}
