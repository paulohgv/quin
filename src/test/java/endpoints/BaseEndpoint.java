package endpoints;

import enums.RequestType;
import groovyjarjarantlr4.v4.runtime.misc.NotNull;
import groovyjarjarantlr4.v4.runtime.misc.Nullable;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import pojo.request.BinRequest;

import java.security.InvalidParameterException;

public class BaseEndpoint extends AbstractBaseEndpoint{

    private RequestSpecification baseRequest(String key){

       return RestAssured.
                given().
                header("X-Master-Key", key).
                contentType(ContentType.JSON);
    }

    private Response executePost(BinRequest binRequest, String key){
        if (binRequest == null){
            throw new InvalidParameterException("Bins obj is null");
        }

        return  baseRequest(key).
                body(binRequest).
                when().
                post(baseUrl + route);
    }

    private Response executePost(String binString, String key){

        return  baseRequest(key).
                body(binString).
                when().
                post(baseUrl + route);
    }

    private Response executePut(BinRequest binRequest, String binsId, String key){
        if (binRequest == null){
            throw new InvalidParameterException("Bins obj is null");
        }

        return  baseRequest(key).
                body(binRequest).
                when().
                put(baseUrl + route + binsId);
    }

    private Response executePut(String binString, String binsId, String key){

        return  baseRequest(key).
                body(binString).
                when().
                put(baseUrl + route + binsId);
    }


    private Response executeGet(String binsId, String key){
        if (binsId == null){
            throw new InvalidParameterException("binsId is null.");
        }

        return  baseRequest(key).
                when().
                get(baseUrl + route + binsId);
    }


    private Response executeDelete(String binsId, String key){
        if (binsId == null){
            throw new InvalidParameterException("binsId is null.");
        }

        return  baseRequest(key).
                when().
                delete(baseUrl + route + binsId);
    }

    protected Response sendRequest(@NotNull RequestType type, @Nullable Object binObj, String binsId, Boolean isFakeKey) {
        Response response = null;
        String key = (isFakeKey == false ? API_KEY : FAKE_KEY);
        BinRequest bin;
        String binStr;

        switch (type) {
            case POST:
                if(binObj instanceof BinRequest){
                    response = executePost((BinRequest) binObj, key);
                } else if (binObj instanceof String){
                    response = executePost((String) binObj, key);
                }
                break;
            case GET:
                response = executeGet(binsId, key);
                break;
            case PUT:
                if(binObj instanceof BinRequest){
                    response = executePut((BinRequest) binObj,binsId , key);
                } else if (binObj instanceof String){
                    response = executePut((String) binObj, binsId, key);
                }
                break;
            case DELETE:
                response = executeDelete(binsId, key);
                break;
        }

        return response;
    }
}
