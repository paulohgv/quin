package endpoints;

import enums.RequestType;
import io.restassured.response.Response;

public class GetBins extends BaseEndpoint {

    private String binId;

    public GetBins(String binId){
       setBinId(binId);
    }

    public String getBinId() {
        return binId;
    }

    public void setBinId(String binId) {
        this.binId = binId;
    }


    public Response getBinById(Boolean isFakeKey){
        return sendRequest(RequestType.GET, null, getBinId(), isFakeKey);
    }
}
