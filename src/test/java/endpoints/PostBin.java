package endpoints;

import enums.RequestType;
import io.restassured.response.Response;
import pojo.request.BinRequest;

public class PostBin extends BaseEndpoint {


    public Response postNewBin(Object bin, Boolean isFakeKey){
        return sendRequest(RequestType.POST, bin, null, isFakeKey);
    }
}
