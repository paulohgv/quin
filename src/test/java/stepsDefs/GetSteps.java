package stepsDefs;

import endpoints.GetBins;
import enums.StatusCode;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import pojo.response.Record;

public class GetSteps extends CommonSteps {

    StepConfig config = new StepConfig();

    @When("I send a Get request with {string} api key")
    public void iSendAndGETRequestUsingAValidId(String value) {
        response = new GetBins(binId).getBinById(config.isFakeKey(value));
    }

    @When("I send a Get request with {string} api key and a invalid binId")
    public void iSendAndGETRequestUsingAInvalidId(String value) {
        response = new GetBins("1234").getBinById(config.isFakeKey(value));
    }

    @And("the id in the response should be the same from I created")
    public void theIdInTheResponseShouldBeTheSameFromICreated() {
        record = response.getBody().as(Record.class);
        Assert.assertEquals(binId, record.getMetadata().getId());
    }

    @Then("the api response should be Unprocessable Entity")
    public void theApiResponseShouldBe() {
        Assert.assertEquals( StatusCode.Unprocessable_Entity.getCode(), response.statusCode());
    }
}
