package stepsDefs;

import endpoints.DeleteBins;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import org.junit.Assert;
import pojo.response.Delete;


public class DeleteSteps extends CommonSteps{

    private Delete delete;

    @When("I send a Delete request with {string} api key")
    public void iSendADeleteRequestWithApiKey(String value) {
        response = new DeleteBins(binId).deleteBinById(config.isFakeKey(value));
    }

    @And("in the response should contain a message {string}")
    public void inTheResponseShouldContainAMessage(String message) {
        delete = response.getBody().as(Delete.class);
        Assert.assertEquals(message, delete.getMessage());
    }

}
