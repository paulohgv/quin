package stepsDefs;

import endpoints.PostBin;
import enums.StatusCode;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.java8.En;
import io.restassured.module.jsv.JsonSchemaValidator;
import pojo.request.BinRequest;
import pojo.response.Record;
import org.junit.Assert;

public class PostSteps extends CommonSteps implements En{

    StepConfig config = new StepConfig();

    @Given("I prepare a bin object")
    public void createBin() {
        objRequest = new BinRequest("random string");
    }

    @When("I send a Post request with {string} api key")
    public void sendPostRequest(String value){
        if (objRequest instanceof BinRequest) {
            response = new PostBin().postNewBin((BinRequest)objRequest, config.isFakeKey(value));
        } else if (objRequest instanceof String){
            response = new PostBin().postNewBin((String)objRequest, config.isFakeKey(value));
        }
    }

    @Then("a new bin should be create with status as SUCCESS")
    @Then("the api response should be SUCCESS")
    public void validatingResponse(){
        Assert.assertEquals( StatusCode.SUCCESS.getCode(), response.statusCode());
    }

    @And("should contain in the response body a valid id")
    public void itShouldContainInTheResponseBodyAnValidId() {
        record = response.getBody().as(Record.class);
        binId = record.getMetadata().getId();
        Assert.assertNotNull(binId);
    }

    @Then("the api response should be UNAUTHORIZED")
    public void theApiResponseShouldBeForbidden() {
        Assert.assertEquals( StatusCode.UNAUTHORIZED.getCode(), response.statusCode());
    }

    @Then("the api response should be BAD REQUEST")
    public void theApiResponseShouldBeBadRequest() {
        Assert.assertEquals( StatusCode.BAD_REQUEST.getCode(), response.statusCode());
    }

    @And("the message error should be {string}")
    public void theMessageErrorShouldBe(String message) {
        errorMessage = response.getBody().as(Error.class);
        Assert.assertEquals(message, errorMessage.getMessage());
    }

    @Given("I prepare an invalid bin object")
    public void iPrepareAnEmptyBinObject() {
        objRequest = "nothing will happen";
    }

    @And("the response body should be equals to {string}")
    public void theResponseBodyShouldBeEqualsToTheSpecification(String path) {
        response.then().assertThat().body(JsonSchemaValidator.matchesJsonSchemaInClasspath(path));
    }

    @And("I clean up my tests")
    public void iCleanUpMyTests() {
        record = null;
        response = null;
        objRequest = null;
        errorMessage = null;
        binId = null;
    }
}

