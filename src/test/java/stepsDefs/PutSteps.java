package stepsDefs;

import endpoints.PostBin;
import endpoints.PutBins;
import io.cucumber.java.en.When;
import pojo.request.BinRequest;

import java.util.Date;

public class PutSteps extends CommonSteps{

    BinRequest bin;

    @When("I send a Put request with {string} api key")
    public void iSendAPutRequestWithApiKey(String value) {
        if (objRequest instanceof BinRequest) {
            response = new PutBins(binId).editBinById((BinRequest)objRequest, config.isFakeKey(value));
        } else if (objRequest instanceof String){
            response = new PutBins(binId).editBinById((String)objRequest, config.isFakeKey(value));
        }
    }

    @When("I prepare a new bin object")
    public void iPrepareANewBinObject(){
        bin = new BinRequest();
        bin.setSample(new Date().toString());
    }
}
