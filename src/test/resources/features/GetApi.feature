Feature: Validating GET API call

  Background: Creating a bin to be used in the tests
    Given I prepare a bin object
    When I send a Post request with "valid" api key
    Then a new bin should be create with status as SUCCESS

  Scenario: Send a get request for a existing bin
    Given should contain in the response body a valid id
    When I send a Get request with "valid" api key
    Then the api response should be SUCCESS
    And the id in the response should be the same from I created
    And I clean up my tests

  Scenario: Send a GET request for a existing bin using an invalid access token
    Given should contain in the response body a valid id
    When I send a Get request with "invalid" api key
    Then the api response should be UNAUTHORIZED
    And the message error should be "X-Master-Key is invalid or the bin doesn't belong to your account"
    And I clean up my tests

  Scenario: Send a GET request for a existing bin and the response is valid GET schema
    Given should contain in the response body a valid id
    When I send a Get request with "valid" api key
    Then the api response should be SUCCESS
    And the response body should be equals to "json/GetResponse.json"
    And I clean up my tests

  Scenario: Send a GET request for a bin that does not exist
    Given I send a Get request with "valid" api key and a invalid binId
    Then the api response should be Unprocessable Entity
    And the message error should be "Invalid Record ID"
    And I clean up my tests
