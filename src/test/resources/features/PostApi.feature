  Feature:  Validating Post API call

    Scenario: I can create a new bin successfully
      Given I prepare a bin object
      When I send a Post request with "valid" api key
      Then a new bin should be create with status as SUCCESS
      And should contain in the response body a valid id
      And I clean up my tests

    Scenario: I can create a new bin successfully and the response request has a valid schema
      Given I prepare a bin object
      When I send a Post request with "valid" api key
      Then a new bin should be create with status as SUCCESS
      And the response body should be equals to "json/BinsResponse.json"
      And I clean up my tests

    Scenario: I can't create a new bin successfully using a fake key
      Given I prepare a bin object
      When I send a Post request with "invalid" api key
      Then the api response should be UNAUTHORIZED
      And the message error should be "Invalid X-Master-Key provided"
      And I clean up my tests

    Scenario: I can't create a new bin successfully with an invalid Json
      Given I prepare an invalid bin object
      When I send a Post request with "valid" api key
      Then the api response should be BAD REQUEST
      And the message error should be "Invalid JSON. Please try again"
      And I clean up my tests