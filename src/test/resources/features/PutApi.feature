Feature: Validating PUT API call

  Background: Creating a bin to be used in the tests
    Given I prepare a bin object
    When I send a Post request with "valid" api key
    Then a new bin should be create with status as SUCCESS

  Scenario: Send PUT request for a existing bin and get it updated
    Given should contain in the response body a valid id
    When I prepare a new bin object
    And I send a Put request with "valid" api key
    Then the api response should be SUCCESS
    And I clean up my tests

  Scenario: Send PUT request for a existing bin and the json schema is valid
    Given should contain in the response body a valid id
    When I prepare a new bin object
    And I send a Put request with "valid" api key
    Then the api response should be SUCCESS
    And the response body should be equals to "json/PutResponse.json"
    And I clean up my tests

  Scenario: Send PUT request for a existing bin with an invalid API key
    Given should contain in the response body a valid id
    When I prepare a new bin object
    And I send a Get request with "invalid" api key
    Then the api response should be UNAUTHORIZED
    And the message error should be "X-Master-Key is invalid or the bin doesn't belong to your account"
    And I clean up my tests

  Scenario: Send PUT request for a existing bin with an invalid Json
    Given should contain in the response body a valid id
    When I prepare an invalid bin object
    And I send a Put request with "valid" api key
    Then the api response should be BAD REQUEST
    And the message error should be "Invalid JSON. Please try again"
    And I clean up my tests