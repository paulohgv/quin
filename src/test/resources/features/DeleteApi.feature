Feature: Validating Delete API call

  Background:  Creating a bin to be used in the tests
    Given I prepare a bin object
    When I send a Post request with "valid" api key
    Then a new bin should be create with status as SUCCESS

  Scenario: Send DELETE request for a existing bin and get it deleted
    Given should contain in the response body a valid id
    When I send a Delete request with "valid" api key
    Then the api response should be SUCCESS
    And in the response should contain a message "Bin deleted successfully"
    And I clean up my tests

  Scenario: Send DELETE request using an invalid token
    Given should contain in the response body a valid id
    When I send a Delete request with "invalid" api key
    Then the api response should be UNAUTHORIZED
    And in the response should contain a message "Invalid X-Master-Key provided"
    And I clean up my tests

  Scenario: Send a DELETE request for a existing bin and the response is valid delete schema
    Given should contain in the response body a valid id
    When I send a Delete request with "valid" api key
    Then the api response should be SUCCESS
    And the response body should be equals to "json/DeleteResponse.json"
    And I clean up my tests