# Quin

This is a project that is based on:

- Rest-Assured (https://rest-assured.io/)
- Cucumber (https://cucumber.io/)

and the tests are running validating the APIs from: https://jsonbin.io/api-reference

As build and depency engine, this project is using:
- Maven (https://maven.apache.org/)
- Gradle (https://gradle.org/)

To execute this project, you should have configured:
- Either Maven or Gradle
- Java JDK 16 (https://www.oracle.com/java/technologies/javase-downloads.html

Running the project:
go to the root folder and:
- `mvn clean test` or, 
- `gradle clean quin`

As an output, you will see the reports integrated in "https://reports.cucumber.io/". Just click on the link that will output in the [Test pipeline](https://gitlab.com/paulohgv/quin/-/pipelines) or go to the Test tab, [also in the pipeline session](https://gitlab.com/paulohgv/quin/-/pipelines), tab to see the results in a junit output.

What needs improvement?
- There are few duplicated code that could be abstracted in a abstract class;
- Due to cucumber lib limitations, I had to create a method to cleanUp the tests instead of using @After. However, the reusability in the tests are way better.
- integrated in a pipeline. (update: friday 18:54 this problem has been fixed)
